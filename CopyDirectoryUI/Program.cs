﻿using CopyDirectory.Interface;
using System;
using System.IO;
using System.Reactive.Linq;
using System.Text.RegularExpressions;

namespace CopyDirectory
{
    public class Program
    {
        static void Main(string[] args)
        {
            string source, destination;
            bool isValid = true;
            bool isValidPath = true;
            do
            {
                Console.WriteLine("Please choose source Path");

                source = Console.ReadLine();

                isValidPath = ValidatePath(source);

                if (isValidPath)
                {
                    isValid = IsValidFilename(source);

                    if (!isValid)
                    {
                        Console.WriteLine("Invalid characters found, Please specify valid path!!");
                    }
                }
               else
                {
                    Console.WriteLine("Invalid source directory Path");
                }
                

            } while (string.IsNullOrEmpty(source.Trim()) || !isValid || !isValidPath);
         
            
            do
            {
                Console.WriteLine("Please Choose Destination Path");

                destination = Console.ReadLine();

                isValidPath = ValidatePath(destination);

                if (isValidPath)
                {
                    isValid = IsValidFilename(destination);

                    if (!isValid)
                    {
                        Console.WriteLine("Invalid characters found, Please specify valid path!!");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid destination directory Path");
                }

            } while (string.IsNullOrEmpty(destination.Trim()) || !isValid || !isValidPath);
            
            IDirectoryCopy _directoryCopy = new DirectoryCopy();

            _directoryCopy.Progress.Subscribe(Console.WriteLine);

            _directoryCopy.CopyDirectory(source, destination);

            Console.ReadLine();
        }

        static bool ValidatePath(string source)
        {
            bool isValid = true;
            try
            {
                Path.GetFullPath(source);
            }
            catch (Exception)
            {
                isValid = false;
            }
            return isValid;
        }

        static bool IsValidFilename(string testName)
        {
            Regex containsABadCharacter = new Regex("["
                  + Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };
            
            return true;
        }
    }

    
}
