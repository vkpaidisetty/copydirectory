﻿using CopyDirectory.Interface;
using System;
using System.IO;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace CopyDirectory
{
    public class DirectoryCopy : IDirectoryCopy
    {
        private Subject<string> _subject;

        public IObservable<string> Progress {
            get {
                return _subject;
            }
        }

        public DirectoryCopy()
        {
            _subject = new Subject<string>();
        }

        public async Task CopyDirectory(string sourceDirectory, string targetDirectory)
        {
            try
            {
                foreach (var file in Directory.EnumerateFiles(sourceDirectory, "*", SearchOption.TopDirectoryOnly))
                {
                    try
                    {
                        //Continue if there isn't any 
                        if (file == null) continue;
                        //Opens an existing file for reading
                        using (var openNReadFile = File.OpenRead(file))
                        {
                            using (var ReadNWriteFile = File.Open(Path.Combine(targetDirectory, Path.GetFileName(file)), FileMode.Create, FileAccess.ReadWrite))
                            {
                                openNReadFile.CopyTo(ReadNWriteFile, 41943040);
                                
                                _subject.OnNext($"Copying file {Path.GetFileName(file)} to destination {targetDirectory}...");
                            }                               
                        }                           
                    }
                    catch (Exception ex)
                    {
                        _subject.OnNext($"Error whilst copying : Couldn't copy file {file} | {ex.Message}");
                    }
                };

                foreach (var directory in Directory.EnumerateDirectories(sourceDirectory, "*", SearchOption.TopDirectoryOnly))
                {
                    var directoryName = Path.GetFileName(directory);

                    if (!Directory.Exists(Path.Combine(targetDirectory, directoryName)))
                    {
                        Directory.CreateDirectory(Path.Combine(targetDirectory, directoryName));

                        _subject.OnNext($"Creating Directory {directoryName} at destination {targetDirectory}...");
                    }else
                    {
                        _subject.OnNext($"Directory {directoryName} already exists!!");
                    };

                    await CopyDirectory(directory, Path.Combine(targetDirectory, directoryName));
                };
            }
            catch (Exception ex)
            {
                _subject.OnNext($"Error whilst Copying : Couldn't open directory {sourceDirectory} | {ex.Message}");
            }
            
        }
    }
}
