﻿using System.Threading.Tasks;
using System;

namespace CopyDirectory.Interface
{
    public interface IDirectoryCopy
    {
        IObservable<string> Progress { get; }

        Task CopyDirectory(string sourceDirectory, string targetDirectory);        
    }
}
